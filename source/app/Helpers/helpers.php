<?php

use Illuminate\Support\Collection;
/**
 * Predefined Messages
 */
define('SUCCESS', 'Successfully created');
define('FAIL', 'Failed to create');
define('UPDATE_SUCCESS', 'Successfully updated');
define('UPDATE_FAIL', 'Failed to update');
define('SERVER_ERROR', 'Internal server error!');
define('DELETE_SUCCESS', 'Successfully deleted');
define('DELETE_FAIL', 'Failed to delete');
define('UNAUTHORIZED', 'These credentials do not match our records.');
define('PERMISSION_DENIED', 'Insufficient Permissions!');
define('PAGINATE_LIMIT', 10);

/**
 * Flash message with label
 * @Param String $message
 * @param String $level ='info'
 * @Return null
 */
if (!function_exists('flash')) {
    function flash($message, $level = 'success', $important = false)
    {
        session()->flash('flash_message', $message);
        session()->flash('flash_message_level', $level);
        session()->flash('flash_important', $important);
    }
}

/**
 * Remove spaces from string
 */
if (!function_exists('removeSpace')) {
    function removeSpace($str, $replaceBy = '')
    {
        return preg_replace('/\s+/', $replaceBy, $str);
    }
}

/**
 * Common json response with datas
 */
if (!function_exists('respond')) {
    function respond($data, $key = 'data', $code = 200, $status = true)
    {
        return response()->json([
            'success' => $status,
            "{$key}" => $data,
        ], $code);
    }
}

/**
 * Common json success response
 */
if (!function_exists('respondSuccess')) {
    function respondSuccess($message, $code = 200, $status = true)
    {
        return response()->json([
            'success' => $status,
            'message' => $message
        ], $code);
    }
}

/**
 * Common json error response
 */
if (!function_exists('respondError')) {
    function respondError($message, $code = 500, $status = false)
    {
        return response()->json([
            'success' => $status,
            'message' => $message
        ], $code);
    }
}

/**
 * Date Format
 */
if (!function_exists('dateFormat')) {
    function dateFormat($date, $format = 'l, d F Y', $replaceBy = null)
    {
        if (!empty($date) && !empty($replaceBy)) {
            $date = str_replace('/', $replaceBy, $date);
        }
        return $date ? date($format, strtotime($date)) : null;
    }
}


if (!function_exists('contentCollection')) {
    function contentCollection()
    {
        return $documents = collect([
            [
                'tag' => ' COVID-19, Coronavirus, disease',//'COVID-19 , Coronavirus , disease'
                'author' => 'Nicole Tumiati (M & A)',
                'title' => ' Coronavirus disease (COVID-19)',
                'description' => "On this website you can find information and guidance from WHO regarding the current outbreak of coronavirus disease (COVID-19) that was first reported from Wuhan, China, on 31 December 2019. Please visit this page for daily updates",
            ],
            [
                'tag' => ' Coronavirus, disease',
                'author' => 'Nicole Tumiati (M & A)',
                'title' => 'Coronavirus Description',
                'description' => "Coronaviruses are a group of related RNA viruses that cause diseases in mammals and birds. In humans, these viruses cause respiratory tract infections that can range from mild to lethal.",
            ],
            [
                'tag' => ' Epidemic , disease , crisis',
                'author' => 'Nicole Tumiati (M & A)',
                'title' => 'Epidemic Situation',
                'description' => "An epidemic is the rapid spread of disease to a large number of people in a given population within a short period of time. For example, in meningococcal infections, an attack rate in excess of 15 cases per 100,000 people for two consecutive weeks is considered an epidemic.",
            ],
            [
                'tag' => 'Vaccine, Medicine, disease',
                'author' => 'Jonas Salk',
                'title' => 'Vaccine',
                'description' => "A vaccine is a biological preparation that provides active acquired immunity to a particular infectious disease. A vaccine typically contains an agent that resembles a disease-causing microorganism and is often made from weakened or killed forms of the microbe",
            ],
            [
                'tag' => 'Payment',
                'author' => 'Nicole Tumiati (M & A)',
                'title' => '(Clause) Non-complete Clause',
                'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            ],
            [
                'tag' => 'Shareholders',
                'author' => 'Nicole Tumiati (M & A)',
                'title' => '(Clause) Non-complete Clause',
                'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
            ],
        ]);
    }
}
