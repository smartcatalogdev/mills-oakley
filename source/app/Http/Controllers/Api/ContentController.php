<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function getContent($query = null)
    {
        $content = contentCollection()->filter(function ($item) use ($query){
            if (strpos($item['tag'], $query)) {
                return $item;
            } else if (strpos($item['title'], $query)) {
                return $item;
            }
        });
        return json_encode($content);
    }
}
