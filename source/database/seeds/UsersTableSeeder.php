<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Super admin',
            'email' => 'superadmin@gmail.com',
            'password' => bcrypt('p@ssword'),
            'role' => 'super_admin',
            'status' => 'active',
            'email_verified_at' => now()
        ]);

        factory(User::class, 50)->create();
    }
}
