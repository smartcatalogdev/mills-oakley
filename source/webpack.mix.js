const mix = require('laravel-mix');

mix.setPublicPath('../');

mix.js('resources/js/app.js', 'js')
    .scripts([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/popper.js/dist/umd/popper.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'resources/js/template-libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js',
        'resources/js/template-libs/app.js',
        'resources/js/template-libs/app.init.light-sidebar.js',
        'resources/js/template-libs/app-style-switcher.js',
        'resources/js/template-libs/waves.js',
        'resources/js/template-libs/sidebarmenu.js',
        'resources/js/template-libs/custom.js',
        'resources/js/template-libs/sparkline/sparkline.js',
        'resources/js/template-libs/chartist/dist/chartist.min.js',
        'resources/js/template-libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js',
        'resources/js/template-libs/c3/d3.min.js',
        'resources/js/template-libs/c3/c3.min.js',
        'resources/js/template-libs/chart.js/dist/Chart.min.js',
        'resources/js/template-libs/dashboards/dashboard1.js',
    ], '../js/app.init.js')
   .sass('resources/sass/app.scss', 'css');

if (mix.inProduction()) {
    mix.version();
}
