<!doctype html>
<html dir="ltr" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MILLS-OAKLEY') }} @yield('title')</title>

    <!-- Fonts -->
    {{-- <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}

    <!-- Styles -->
    <link href="{{ asset('css/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/c3.min.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    
    @yield('style')
</head>
<body>
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Preloader end -->
    <!-- ============================================================== -->

    <div id="app">
        @guest
            <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url(/images/auth-bg.png) no-repeat center center;">
                <div class="auth-box">
                    <div class="logo">
                        <span class="db"><img src="/images/logo/logo-icon.png" alt="logo" /></span>
                    </div>

                    @yield('content')
                </div>
            </div>
        @else
            <!-- ============================================================== -->
            <!-- Main wrapper - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <div id="main-wrapper">
                @include('layouts.partials._topnav')
                @include('layouts.partials._sidebar')

                <div class="page-wrapper">
                    <div class="page-breadcrumb">
                        <div class="row">
                            <div class="col-5 align-self-center">
                                <h4 class="page-title">Dashbaord</h4>
                            </div>
                            <div class="col-7 align-self-center">
                                <div class="d-flex no-block justify-content-end align-items-center">
                                    <div class="mr-2">
                                        <div class="lastmonth"></div>
                                    </div>
                                    <div class="">
                                        <small>Starter Page</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="container-fluid">
                        @yield('content')
                    </div>

                    <footer class="footer d-flex justify-content-between align-items-center">
                        <span></span>
                        <span><a href="#">Millsoakley.com</a>&copy;{{ date('Y') }}. All Rights Reserved Privacy Policy</span>
                        <span class="d-flex align-items-center">Powered By: <img class="ml-1" src="{{ asset('images/logo/logo-footer.png') }}" alt=""></span>
                    </footer>
                </div>
            </div>
        @endguest
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/app.init.js') }}"></script>
    <script src="{{ mix('js/app.js') }}" defer></script>

    @yield('script')
</body>
</html>
