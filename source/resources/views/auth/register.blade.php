@extends('layouts.app')

@section('content')
    <p class="text-center mb-0 pt-2 font-weight-bold h5">Sign Up</p>

    <!-- Register Form -->
    <div class="auth-box-inner">
        <div class="row">
            <div class="col-12">
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="form-group">
                        <label class="font-weight-bold" for="name">{{ __('Name') }}</label>
                        <input 
                            id="name" 
                            type="text" 
                            class="form-control @error('name') is-invalid @enderror" 
                            name="name" 
                            value="{{ old('name') }}" 
                            placeholder="Name"
                            autocomplete="name" 
                            autofocus
                            required 
                        >

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold" for="email">{{ __('E-Mail Address') }}</label>
                        <input 
                            id="email" 
                            type="email" 
                            class="form-control @error('email') is-invalid @enderror" 
                            name="email" 
                            value="{{ old('email') }}" 
                            placeholder="{{ __('E-Mail Address') }}"
                            required 
                        >

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold" for="password">{{ __('Password') }}</label>
                        <input 
                            id="password" 
                            type="password" 
                            class="form-control @error('password') is-invalid @enderror" 
                            name="password" 
                            required 
                            placeholder="********"
                            autocomplete="new-password"
                        >

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold" for="password-confirm">{{ __('Confirm Password') }}</label>
                        <input 
                            id="password-confirm"
                            type="password" 
                            class="form-control @error('password') is-invalid @enderror" 
                            name="password_confirmation" 
                            placeholder="********"
                            autocomplete="new-password"
                            required 
                        >
                    </div>

                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                            CREATE ACCOUNT
                        </button>
                    </div>

                    <div class="d-flex justify-content-center align-items-center py-2">
                        <span>Already have an account? <a href="{{ route('login') }}">Log In</a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
