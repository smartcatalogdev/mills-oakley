@extends('layouts.app')

@section('content')
    <p class="text-center mb-0 pt-2 font-weight-bold">Welcome to Mills Oakley</p>

    <!-- Login Form -->
    <div class="auth-box-inner">
        <div class="row">
            <div class="col-12">
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="form-group">
                        <label class="font-weight-bold" for="email">Email</label>

                        <input 
                            id="email" 
                            type="email" 
                            class="form-control @error('email') is-invalid @enderror" 
                            name="email" 
                            value="{{ old('email') }}" 
                            required 
                            autocomplete="email" 
                            placeholder="Email"
                            autofocus
                        >

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold" for="password">{{ __('Password') }}</label>

                        <input 
                            id="password" 
                            type="password" 
                            class="form-control @error('password') is-invalid @enderror" 
                            name="password" 
                            required 
                            placeholder="********"
                            autocomplete="current-password"
                        >

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    {{-- <div class="form-group row">
                        <div class="col-md-6 offset-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                    </div> --}}

                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                            LOGIN
                        </button>

                            {{-- @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif --}}
                    </div>

                    <div class="d-flex justify-content-between align-items-center py-2">
                        <span>Don’t have an account? <a href="{{ route('register') }}">Sign up</a></span>
                        @if (Route::has('password.request'))
                            <a class="" href="{{ route('password.request') }}">Forgot Password?</a>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
