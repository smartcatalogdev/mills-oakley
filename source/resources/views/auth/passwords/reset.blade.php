@extends('layouts.app')

@section('content')
    <p class="text-center mb-0 pt-2 font-weight-bold h5">{{ __('Reset Password') }}</p>

    <!-- Reset Form -->
    <div class="auth-box-inner">
        <div class="row">
            <div class="col-12">
                <form method="POST" action="{{ route('password.update') }}">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group">
                        <label class="font-weight-bold" for="email">Email</label>
                        <input 
                            id="email" 
                            type="email" 
                            class="form-control @error('email') is-invalid @enderror" 
                            name="email" 
                            value="{{ old('email') }}" 
                            required 
                            autocomplete="email" 
                            placeholder="Email"
                            autofocus
                        >

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold" for="password">{{ __('Password') }}</label>

                        <input 
                            id="password" 
                            type="password" 
                            class="form-control @error('password') is-invalid @enderror" 
                            name="password" 
                            required 
                            placeholder="********"
                            autocomplete="new-password"
                        >

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold" for="password-confirm">{{ __('Confirm Password') }}</label>
                        <input 
                            id="password-confirm"
                            type="password" 
                            class="form-control @error('password') is-invalid @enderror" 
                            name="password_confirmation" 
                            placeholder="********"
                            autocomplete="new-password"
                            required 
                        >
                    </div>

                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                            {{ __('Reset Password') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
