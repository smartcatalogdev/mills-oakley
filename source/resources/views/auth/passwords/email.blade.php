@extends('layouts.app')

@section('content')
    <p class="text-center mb-0 pt-2 font-weight-bold h5">{{ __('Reset Password') }}</p>
    <p class="text-center mb-0 pt-1 small text-secondary">Please enter your email address to request a <br/> password reset.</p>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <!-- Reset Form -->
    <div class="auth-box-inner">
        <div class="row">
            <div class="col-12">
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group">
                        <label class="font-weight-bold" for="email">{{ __('E-Mail Address') }}</label>
                        <input 
                            id="email" 
                            type="email" 
                            class="form-control @error('email') is-invalid @enderror" 
                            name="email" 
                            value="{{ old('email') }}" 
                            required 
                            autocomplete="email" 
                            placeholder="Email"
                            autofocus
                        >

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
